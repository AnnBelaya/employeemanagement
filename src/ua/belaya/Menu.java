package ua.belaya;

import ua.belaya.logic.*;
import ua.belaya.ui.Visualizer;

public class Menu {
    private DataReader dataReader;
    private Visualizer visualizer;
    private EmployeeManager employeeManager;

    public Menu(DataReader dataReader, Visualizer visualizer) {
        this.dataReader = dataReader;
        this.visualizer = visualizer;
        employeeManager = new EmployeeManager(dataReader, visualizer);
    }

    public void menuAction() {
        boolean loop = true;

        while (loop){
            int choice = makeChoseForMenuAction();

            switch (choice) {
                case 1: {
                    employeeManager.addEmployee();
                    break;
                }
                case 2: {
                    employeeManager.findEmployee();
                 break;
                }
                case 3: {
                    employeeManager.departmentOfEmployee();
                    break;
                }
                case 4: {
                    employeeManager.changeData();
                    break;
                }
                case 5: {
                    employeeManager.allEmployees();
                    break;
                }
                case 6: {
                    employeeManager.deleteEmployee();
                    break;
                }
                case 7: {
                    employeeManager.saveToTxtFile();
                    break;
                }
                case 8: {
                    employeeManager.loadTxtFile();
                    break;
                }
                case 9: {
                    employeeManager.loadXmlFile();
                    break;
                }
                case 10:{
                    loop = false;
                }
            }
        }
    }

    private int makeChoseForMenuAction() {
        boolean loop = true;
        int choice = 0;

        while (loop) {
            visualizer.showConsoleMenu();

            try {
                choice = Integer.parseInt(dataReader.readNumber());
            }
            catch (NumberFormatException e) {
                visualizer.showNecessityOfIncludingNumber();
                continue;
            }

            if (choice < 1 || choice > 10) {
                visualizer.showWrongValue();
                continue;
            }

            loop = false;
        }

        return choice;
    }
}
