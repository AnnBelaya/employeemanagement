package ua.belaya;

import ua.belaya.logic.ConsoleDataReader;
import ua.belaya.logic.DataReader;
import ua.belaya.ui.ConsoleVisualizer;
import ua.belaya.ui.Visualizer;

public class EmployeeManagerLauncher {
    public static void main(String[] args) {
        Visualizer visualizer = new ConsoleVisualizer();
        DataReader dataReader = new ConsoleDataReader();

        Menu start = new Menu(dataReader,visualizer);
        start.menuAction();
    }
}
