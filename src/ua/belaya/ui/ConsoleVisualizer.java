package ua.belaya.ui;

import ua.belaya.entity.Employee;

public class ConsoleVisualizer implements  Visualizer{
    private String separator;

    public ConsoleVisualizer(){
        separator = "------------------------------------------------";
    }

    public void showJobTitlesMenu(){
        System.out.println("enter job title: \n"
                + "1: manager \n"
                + "2: developer \n"
                + "3: cleaner \n"
                + "4: back to menu");
    }

    public void showNecessityOfIncludingNumber(){
        System.out.println(separator + "\n" + "enter number \n");
    }

    public void showWrongValue(){
        System.out.println(separator + "\n" + "wrong value \n");
    }

    public void showNecessityOfIncludingWord(){
        System.out.println(separator + "enter word, not number \n");
    }

    public void showNecessityOfIncludingName(){
        System.out.println("enter name \n");
    }

    public void showNecessityOfIncludingSurname(){
        System.out.println("enter surname \n");
    }

    public void showNecessityOfIncludingAge(){
        System.out.println("enter age \n");
    }

    public void showNecessityOfIncludingRightAge(){
        System.out.println("enter right age (employee should be over 18) \n");
    }

    public void showNecessityOfIncludingSalary(){
        System.out.println("enter salary \n");
    }

    public void showNecessityOfIncludingAmountOfDoneWork(String jobTitle){
        switch(jobTitle){
            case "manager":{
                System.out.println("enter amount of sales \n");
                break;
            }
            case "developer":{
                System.out.println("enter amount written lines of code \n");
                break;
            }
            case "cleaner":{
                System.out.println("enter amount cleaned offices \n");
            }
        }
    }

    public void showNecessityOfIncludingCityName(){
        System.out.println("enter city of department: \n"
                + "1 : kiev \n"
                + "2 : dnepropetrovsk \n"
                + "3 : lvov \n");
    }

    public void showNecessityOfIncludingIndexOfEmployee(){
        System.out.println("enter index of employee \n");
    }

    public void showNecessityOfIncludingIndexForDelete(){
        System.out.println("if you want to delete employee enter index, else enter -1 \n");
    }

    public void showChangeDataMenu(){
        System.out.println("change: \n"
                + "1 : name \n"
                + "2 : surname \n"
                + "3 : age \n"
                + "4 : salary \n"
                + "5 : department \n"
                + "6 : back to menu \n");
    }

    public void showFindEmployeeByMenu(){
        System.out.println("find employee \n"
                + "1: by name \n"
                + "2: by surname \n"
                + "3: by name and surname \n"
                + "4: back to menu \n");
    }

    public void noticeOfTheAddition(){
        System.out.println("the employee is added \n");
    }

    public void noticeOfRemoval(){
        System.out.println("the employee is deleted \n");
    }

    public void showConsoleMenu(){
        System.out.println(	separator + "\n" + "1: add employee" + "\n"
                + separator + "\n" + "2: find employee" + "\n"
                + separator + "\n" + "3: see department of employee" + "\n"
                + separator + "\n" + "4: change information about employee" + "\n"
                + separator + "\n" + "5: see all employees" + "\n"
                + separator + "\n" + "6: delete employee" + "\n"
                + separator + "\n" + "7: save employees to txt file" + "\n"
                + separator + "\n" + "8: load employees from txt file" + "\n"
                + separator + "\n" + "9: load employees from xml file" + "\n"
                + separator + "\n" + "10: close menu" + "\n"
                + separator);
    }

    public void showEmptyList(){
        System.out.println("no employees \n");
    }

    public void showAllAboutEmployee(Employee employee){
        System.out.println(employee.toString());
    }

    public void showListOfEmployees(Employee employee, int index){
        System.out.println("index: " + index + " employee: " + employee.getName() + " " + employee.getSurname() + "\n");
    }

    public void showDepartmentOfEmployee(Employee employee){
        System.out.println("department: " + employee.getCity().toString() + ", " + employee.getCity().departmentsLocation() + "\n");
    }

    public void showDirectionForLoadMenu(){
        System.out.println("load from \n"
                + "1: MyEmployeeList.txt \n"
                + "2: own direction \n");
    }

    public void showDirectionToSaveMenu(){
        System.out.println("save to \n"
                + "1: EmployeeList.txt \n"
                + "2: own direction \n");
    }

    public void showDirectionForLoadXml(){
        System.out.println("load from \n"
                + "1: Employees.xml \n"
                + "2: own direction \n");
    }

    public void showCantLoadFile(){
        System.out.println("File can`t be loaded");
    }

    public void showCantSaveFile(){
        System.out.println("File can`t be saved");
    }

    public void showLoadedFile(){
        System.out.println("File is loaded");
    }

    public void showSavedFile(){
        System.out.println("File is saved");
    }
}
