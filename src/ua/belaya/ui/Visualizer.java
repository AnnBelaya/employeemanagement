package ua.belaya.ui;

import ua.belaya.entity.Employee;

public interface Visualizer {
    void showJobTitlesMenu();
    void showNecessityOfIncludingNumber();
    void showWrongValue();
    void showNecessityOfIncludingRightAge();
    void showNecessityOfIncludingWord();
    void showNecessityOfIncludingName();
    void showNecessityOfIncludingSurname();
    void showNecessityOfIncludingAge();
    void showNecessityOfIncludingSalary();
    void showNecessityOfIncludingAmountOfDoneWork(String jobTitle);
    void showNecessityOfIncludingCityName();
    void showNecessityOfIncludingIndexOfEmployee();
    void showChangeDataMenu();
    void showFindEmployeeByMenu();
    void noticeOfTheAddition();
    void showEmptyList();
    void showAllAboutEmployee(Employee employee);
    void showListOfEmployees(Employee employee, int index);
    void showDepartmentOfEmployee(Employee employee);
    void showDirectionForLoadMenu();
    void showDirectionToSaveMenu();
    void showDirectionForLoadXml();
    void showCantLoadFile();
    void showCantSaveFile();
    void showLoadedFile();
    void showSavedFile();
    void showNecessityOfIncludingIndexForDelete();
    void noticeOfRemoval();
    void showConsoleMenu();
}
