package ua.belaya.logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TxtWriter {
    public void saveToTxtFile(String allEmployees, String direction)throws IOException{
            File file = new File(direction);
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.close();

            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.append(allEmployees);
            writer.close();
    }
}
