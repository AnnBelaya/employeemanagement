package ua.belaya.logic;

import ua.belaya.EmployeeManager;
import ua.belaya.entity.Department;
import ua.belaya.entity.Employee;
import ua.belaya.ui.Visualizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;

public class TxtReader {
    private EmployeeManager employeeManager;

    public TxtReader(DataReader dataReader, Visualizer visualizer) {
        employeeManager = new EmployeeManager(dataReader, visualizer);
    }

    public ArrayList<Employee> loadTxtFile(ArrayList<Employee> employees, String direction) throws IOException {
        StringBuilder lineReader = new StringBuilder();
        File file = new File(direction);
        FileReader fileReader = new FileReader(file);
        fileReader.close();
        BufferedReader reader = new BufferedReader(fileReader);

        String line;

        while ((line = reader.readLine()) != null) {
            lineReader.append(line);
        }
        reader.close();


        String[] result = lineReader.toString().split("/");

        for (String aResult : result) {
            String[] part = aResult.split(",");

            int j = 0;

            String jobTitle = part[0];

            if (!"manager".equals(jobTitle) & !"developer".equals(jobTitle) & !"cleaner".equals(jobTitle)) {
                throw new InputMismatchException();
            }

            String name = part[1];
            String surname = part[j + 2];
            int age = Integer.parseInt(part[j + 3]);
            int salary = Integer.parseInt(part[j + 4]);
            int amountOfDoneWork = Integer.parseInt(part[j + 5]);
            Department cityOfDepartment;

            switch (part[j + 6]) {
                case "kiev": {
                    cityOfDepartment = Department.KIEV;
                    break;
                }
                case "dnepropetrovsk": {
                    cityOfDepartment = Department.DNEPROPETROVSK;
                    break;
                }
                case "lvov": {
                    cityOfDepartment = Department.LVOV;
                    break;
                }
                default: {
                    throw new InputMismatchException();
                }
            }
            employees.add(employeeManager.employeeBuilder(jobTitle, name, surname, age, salary, amountOfDoneWork, cityOfDepartment));
        }
        return employees;
    }
}
