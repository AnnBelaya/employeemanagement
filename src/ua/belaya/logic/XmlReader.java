package ua.belaya.logic;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ua.belaya.EmployeeManager;
import ua.belaya.entity.Department;
import ua.belaya.entity.Employee;
import ua.belaya.ui.Visualizer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.InputMismatchException;

public class XmlReader {
    private EmployeeManager employeeManager;

    public XmlReader(DataReader dataReader, Visualizer visualizer){
        employeeManager = new EmployeeManager(dataReader,visualizer);
    }

    public ArrayList<Employee> readXmlFile(ArrayList<Employee> employees, String direction)throws Exception {
        ArrayList<Employee> newEmployees = employees;

        DocumentBuilder xml = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = xml.parse(new File(direction));
        NodeList list = doc.getElementsByTagName("employee");

        for(int i = 0; i < list.getLength(); i++){
            Node node = list.item(i);
            Element element = (Element) node;

            String jobTitle = element.getElementsByTagName("jobTitle").item(0).getTextContent();

            if(!"manager".equals(jobTitle) & !"developer".equals(jobTitle) & !"cleaner".equals(jobTitle)){
                throw new InputMismatchException();
            }

            String name = element.getElementsByTagName("name").item(0).getTextContent();
            String surname = element.getElementsByTagName("surname").item(0).getTextContent();
            int age = Integer.parseInt(element.getElementsByTagName("age").item(0).getTextContent());
            int salary = Integer.parseInt(element.getElementsByTagName("salary").item(0).getTextContent());
            int amountOfDoneWork = Integer.parseInt(element.getElementsByTagName("amountOfDoneWork").item(0).getTextContent());

            Department cityOfDepartment;

            switch(element.getElementsByTagName("department").item(0).getTextContent()){
                case "kiev":{
                    cityOfDepartment = Department.KIEV;
                    break;
                }
                case "dnepropetrovsk":{
                    cityOfDepartment = Department.DNEPROPETROVSK;
                    break;
                }
                case "lvov":{
                    cityOfDepartment = Department.LVOV;
                    break;
                }
                default:{
                    throw new InputMismatchException();
                }
            }
            newEmployees.add(employeeManager.employeeBuilder(jobTitle, name, surname, age, salary, amountOfDoneWork, cityOfDepartment));
        }
        return newEmployees;
    }
}
