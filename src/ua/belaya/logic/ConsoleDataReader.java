package ua.belaya.logic;

import ua.belaya.entity.*;
import java.util.Scanner;

public class ConsoleDataReader implements DataReader{
    private String direction;
    private Scanner scanner;

    public ConsoleDataReader(){
        scanner = new Scanner(System.in);
    }

    public String readJobTitle(){
        String jobTitle;
        String choice = scanner.nextLine();

        switch(choice){
            case "1":{
                jobTitle = "manager";
                break;
            }
            case "2":{
                jobTitle = "developer";
                break;
            }
            case "3":{
                jobTitle = "cleaner";
                break;
            }
            case "4":{
                jobTitle = "end";
                break;
            }
            default:{
                jobTitle = "mistake";
            }
        }

        return jobTitle;
    }

    public String readName(){
        return scanner.nextLine();
    }

    public String readSurname(){
        return scanner.nextLine();
    }

    public String readNumber(){
        return scanner.nextLine();
    }

    public Department readCityOfDepartment(){
        Department cityOfDepartment = null;

        int choice = scanner.nextInt();

        switch(choice){
            case 1:{
                cityOfDepartment = Department.KIEV;
                break;
            }
            case 2:{
                cityOfDepartment = Department.DNEPROPETROVSK;
                break;
            }
            case 3:{
                cityOfDepartment = Department.LVOV;
            }
        }
        return cityOfDepartment;
    }

    public String readDirectionToSaveTxt(int choice){
        switch(choice){
            case 1:{
                direction = "D:/EmployeeManagement1/src/resources/EmployeeList.txt";
                break;
            }
            case 2:{
                direction = scanner.nextLine();
                break;
            }
            default:{
                direction = "wrong direction";
            }
        }

        return direction;
    }

    public String readDirectionForLoadTxt(int choice){
        switch(choice){
            case 1:{
                direction = "D:/EmployeeManagement1/src/resources/MyEmployeeList.txt";
                break;
            }
            case 2:{
                direction = scanner.nextLine();
                break;
            }
            default:{
                direction = "wrong direction";
            }
        }

        return direction;
    }

    public String readDirectionForLoadXml(int choice){
        switch(choice){
            case 1:{
                direction = "D:/EmployeeManagement1/src/resources/Employees.xml";
                break;
            }
            case 2:{
                direction = scanner.nextLine();
                break;
            }
            default:{
                direction = "wrong";
            }
        }

        return direction;
    }
}
