package ua.belaya.logic;

import ua.belaya.entity.Department;

public interface DataReader {
    String readJobTitle();
    String readName();
    String readNumber();
    String readSurname();
    Department readCityOfDepartment();
    String readDirectionForLoadTxt(int choice);
    String readDirectionToSaveTxt(int choice);
    String readDirectionForLoadXml(int choice);
}
