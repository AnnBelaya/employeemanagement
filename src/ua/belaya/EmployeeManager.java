package ua.belaya;

import ua.belaya.entity.*;
import ua.belaya.logic.*;
import ua.belaya.ui.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;

public class EmployeeManager {
    private DataReader dataReader;
    private Visualizer visualizer;
    private ArrayList<Employee> employees;
    private Employee employee;

    public EmployeeManager(DataReader dataReader, Visualizer visualizer) {
        this.dataReader = dataReader;
        this.visualizer = visualizer;
        employees = new ArrayList<>();
    }

    public void addEmployee() {
        String jobTitle = readJobTitle();

        if ("end".equals(jobTitle)) {
            return;
        }

        String name = readName();
        String surname = readSurname();

        visualizer.showNecessityOfIncludingAge();
        int age = readAge();

        visualizer.showNecessityOfIncludingSalary();
        int salary = positiveNumber();

        visualizer.showNecessityOfIncludingAmountOfDoneWork(jobTitle);
        int amountOfDoneWork = positiveNumber();

        Department cityOfDepartment = readCity();

        employees.add(employeeBuilder(jobTitle, name, surname, age, salary, amountOfDoneWork, cityOfDepartment));
        visualizer.noticeOfTheAddition();
    }

    public Employee employeeBuilder(String jobTitle, String name, String surname, int age, int salary, int amountOfDoneWork, Department cityOfDepartment) {
        switch (jobTitle) {
            case "manager": {
                employee = new Manager(name, surname, age, salary, amountOfDoneWork, cityOfDepartment);
                break;
            }
            case "developer": {
                employee = new Developer(name, surname, age, salary, amountOfDoneWork, cityOfDepartment);
                break;
            }
            case "cleaner": {
                employee = new Cleaner(name, surname, age, salary, amountOfDoneWork, cityOfDepartment);
            }
        }
        return employee;
    }

    public void allEmployees() {
        if (isEmptyList()) {
            return;
        }

        for(Employee employee : employees){
            visualizer.showAllAboutEmployee(employee);

        }
    }

    public void changeData() {
        if (isEmptyList()) {
            return;
        }

        int choice = makeChoseToChangeData();

        if (choice == -1) {
            return;
        }

        int index = readIndexOfEmployee();

        changeDataOperation(choice, index);
    }

    public void deleteEmployee() {
        if (isEmptyList()) {
            return;
        }

        int index = 0;
        boolean loop = true;

        while (loop) {
            try {
                visualizer.showNecessityOfIncludingIndexForDelete();
                listOfEmployees();
                index = Integer.parseInt(dataReader.readNumber());
            } catch (NumberFormatException e) {
                visualizer.showNecessityOfIncludingNumber();
                continue;
            }

            if (index == -1) {
                return;
            }

            if (index < employees.size()) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        employees.remove(index);
        visualizer.noticeOfRemoval();
    }

    public void departmentOfEmployee() {
        if (isEmptyList()) {
            return;
        }

        boolean loop = true;
        int index = 0;

        while (loop) {
            listOfEmployees();
            visualizer.showNecessityOfIncludingIndexOfEmployee();

            index = positiveNumber();

            if (index < employees.size()) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        employee = employees.get(index);
        visualizer.showDepartmentOfEmployee(employee);
    }

    public void findEmployee() {
        if (isEmptyList()) {
            return;
        }

        int choice = readChoseToFindEmployee();

        if (choice == -1) {
            return;
        }

        int count = findEmployeeOperation(choice);

        if (count == 0) {
            visualizer.showEmptyList();
        }
    }

    public void loadTxtFile() {
        TxtReader reader = new TxtReader(dataReader, visualizer);
        int choice;
        String direction = "";
        boolean loop = true;

        while (loop) {
            visualizer.showDirectionForLoadMenu();

            choice = positiveNumber();
            direction = dataReader.readDirectionForLoadTxt(choice);

            if (!"wrong".equals(direction)) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        try {
            employees = reader.loadTxtFile(employees, direction);
            visualizer.showLoadedFile();
        } catch (IOException e) {
            visualizer.showCantLoadFile();
        }
    }

    public void saveToTxtFile() {
        TxtWriter writer = new TxtWriter();
        int choice;
        String direction = "";
        boolean loop = true;

        while (loop) {
            visualizer.showDirectionToSaveMenu();

            choice = positiveNumber();
            direction = dataReader.readDirectionToSaveTxt(choice);

            if (!"wrong".equals(direction)) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        try {
            writer.saveToTxtFile(readAllEmployees(), direction);
            visualizer.showSavedFile();
        } catch (IOException e) {
            visualizer.showCantSaveFile();
        }
    }

    public void loadXmlFile() {
        XmlReader xmlReader = new XmlReader(dataReader, visualizer);
        int choice;
        String direction = "";
        boolean loop = true;

        while (loop) {
            visualizer.showDirectionForLoadXml();

            choice = positiveNumber();
            direction = dataReader.readDirectionForLoadXml(choice);

            if (!"wrong".equals(direction)) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        try {
            employees = xmlReader.readXmlFile(employees, direction);
            visualizer.showLoadedFile();
        } catch (Exception e) {
            visualizer.showCantLoadFile();
        }
    }

    public String readAllEmployees() {
        StringBuilder allEmployees = new StringBuilder();

        for (int i = 0; i < employees.size(); i++) {
            allEmployees.append(employees.toString() + "\n");
        }

        return allEmployees.toString();
    }

    private String readJobTitle() {
        String jobTitle = "";
        boolean loop = true;

        while (loop) {
            visualizer.showJobTitlesMenu();

            jobTitle = dataReader.readJobTitle();

            if ("manager".equals(jobTitle) || "developer".equals(jobTitle) || "cleaner".equals(jobTitle) || "end".equals(jobTitle)) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        return jobTitle;
    }

    private String readName() {
        boolean loop = true;
        String name = "";

        while (loop) {
            visualizer.showNecessityOfIncludingName();

            name = dataReader.readName();

            if (isWord(name)) {
                loop = false;
            } else {
                visualizer.showNecessityOfIncludingWord();
            }
        }

        return name;
    }

    private String readSurname() {
        boolean loop = true;
        String surname = "";

        while (loop) {
            visualizer.showNecessityOfIncludingSurname();

            surname = dataReader.readName();

            if (isWord(surname)) {
                loop = false;
            } else {
                visualizer.showNecessityOfIncludingWord();
            }
        }

        return surname;
    }

    private Department readCity() {
        boolean loop = true;
        Department city = null;

        while (loop) {
            city = readCityOfDepartment();

            if (city != null) {
                loop = false;
            }
        }

        return city;
    }

    private int makeChoseToChangeData() {
        boolean loop = true;
        int choice = 0;

        while (loop) {
            visualizer.showChangeDataMenu();

            choice = positiveNumber();

            if (choice == 6) {
                return -1;
            }

            if (choice > 0 && choice < 6) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        return choice;
    }

    private int readIndexOfEmployee() {
        boolean loop = true;
        int index = 0;

        while (loop) {
            listOfEmployees();
            visualizer.showNecessityOfIncludingIndexOfEmployee();

            index = positiveNumber();

            if (index < employees.size()) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        return index;
    }

    private void changeDataOperation(int choice, int index) {
        employee = employees.get(index);

        switch (choice) {
            case 1: {
                visualizer.showNecessityOfIncludingName();
                employee.setName(dataReader.readName());
                break;
            }
            case 2: {
                visualizer.showNecessityOfIncludingSurname();
                employee.setSurname(dataReader.readSurname());
                break;
            }
            case 3: {
                visualizer.showNecessityOfIncludingAge();
                employee.setAge(positiveNumber());
                break;
            }
            case 4: {
                visualizer.showNecessityOfIncludingSalary();
                employee.setSalary(positiveNumber());
                break;
            }
            case 5: {
                readCityOfDepartment();
                employee.setCityOfDepartment(null);
                break;
            }
        }
    }

    private int readChoseToFindEmployee() {
        boolean loop = true;
        int choice = 0;

        while (loop) {
            visualizer.showFindEmployeeByMenu();

            choice = positiveNumber();

            if (choice == 4) {
                return -1;
            }

            if (choice > 0 && choice < 4) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        return choice;
    }

    private int findEmployeeOperation(int choice) {
        int count = 0;

        switch (choice) {
            case 1: {
                visualizer.showNecessityOfIncludingName();
                String name = dataReader.readName();

                for (Employee employee : employees) {
                    if (name.equals(employee.getName())) {
                        visualizer.showAllAboutEmployee(employee);
                        count++;
                    }
                }
                break;
            }
            case 2: {
                visualizer.showNecessityOfIncludingSurname();
                String surname = dataReader.readSurname();

                for (Employee employee : employees) {
                    if (surname.equals(employee.getSurname())) {
                        visualizer.showAllAboutEmployee(employee);
                        count++;
                    }
                }
                break;
            }
            case 3: {
                visualizer.showNecessityOfIncludingName();
                String name = dataReader.readName();

                visualizer.showNecessityOfIncludingSurname();
                String surname = dataReader.readSurname();

                for (Employee employee : employees) {
                    if (name.equals(employee.getName()) & surname.equals(employee.getSurname())) {
                        visualizer.showAllAboutEmployee(employee);
                        count++;
                    }
                }
            }
        }

        return count;
    }

    private int positiveNumber() {
        int number = 0;
        boolean loop = true;

        while (loop) {
            try {
                number = Integer.parseInt(dataReader.readNumber());
            } catch (NumberFormatException e) {
                visualizer.showNecessityOfIncludingNumber();
                continue;
            }

            if (number >= 0) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        return number;
    }

    private int readAge() {
        boolean loop = true;
        int age = 0;

        while (loop) {
            age = positiveNumber();

            if (age >= 18) {
                loop = false;
            } else {
                visualizer.showNecessityOfIncludingRightAge();
            }
        }

        return age;
    }

    private Department readCityOfDepartment() {
        Department city = null;
        boolean loop = true;

        while (loop) {
            visualizer.showNecessityOfIncludingCityName();

            try {
                city = dataReader.readCityOfDepartment();
            } catch (InputMismatchException e) {
                visualizer.showNecessityOfIncludingNumber();
                continue;
            }

            if (city != null) {
                loop = false;
            } else {
                visualizer.showWrongValue();
            }
        }

        return city;
    }

    private boolean isWord(String word) {
        for (char i : word.toLowerCase().toCharArray()) {
            if (i < 97 || i > 122) {
                return false;
            }
        }

        return true;
    }

    private boolean isEmptyList() {
        if (employees.isEmpty()) {
            visualizer.showEmptyList();
            return true;
        } else {
            return false;
        }
    }

    private void listOfEmployees() {
        for (int i = 0; i < employees.size(); i++) {
            employee = employees.get(i);
            visualizer.showListOfEmployees(employee, i);
        }
    }
}