package ua.belaya.entity;

public enum Department{
	DNEPROPETROVSK("SomeSt."), 
	KIEV("Deputatskaya"), 
	LVOV("Ploshya rinok");
	
	private String address;
	
	Department(String address){
		this.address = address;
	}
	
	public String departmentsLocation(){
		return "department`s location is: " + address;
	}
}