package ua.belaya.entity;

public class Manager extends Employee{
	private int amountOfSales;
	
	public Manager(String name, String surname, int age, double salary, int amountOfSales, Department city){
		super(name,surname,age,salary,city);
		this.amountOfSales = amountOfSales;
	}

    @Override
	public double calculateSalary(){
		return calculateSalary() / 100 * amountOfSales * 0.2;
	}
}