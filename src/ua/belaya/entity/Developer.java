package ua.belaya.entity;

public class Developer extends Employee{
	private int sourceLinesOfCode;
	
	public Developer(String name, String surname, int age, double salary, int sourceLinesOfCode, Department city){
		super(name,surname,age,salary,city);
		this.sourceLinesOfCode = sourceLinesOfCode;
	}

    @Override
	public double calculateSalary(){
		return sourceLinesOfCode > 1000000 ? calculateSalary() + 1000 : calculateSalary();
	}
}