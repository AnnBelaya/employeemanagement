package ua.belaya.entity;

public abstract class Employee{
	private String name;
	private String surname;
	private int age;
	private double salary;
	private Department city;
	
	public Employee(String name, String surname, int age, double salary, Department city){
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.salary = salary;
		this.city = city;
	}

    public abstract double calculateSalary();

	public void setName(String name){
		this.name = name;
	}
	
	public void setSurname(String surname){
		this.surname = surname;
	}
	
	public void setAge(int age){
		this.age = age;
	}
	
	public void setSalary(int salary){
		this.salary = salary;
	}
	
	public void setCityOfDepartment(Department city){
		this.city = city;
	}

    @Override
    public String toString(){
        return "job title : " + getClass().getName()
                + ", name : " + getName()
                + ", surname : " + getSurname()
                + ", age : " + getAge()
                + ", salary : " + getSalary()
                + ", city : " + getCity() + "\n";
    }

	public String getName(){
		return name;
	}
	
	public String getSurname(){
		return surname;
	}
	
	public int getAge(){
		return age;
	}
	
	public double getSalary(){
		return salary;
	}
	
	public Department getCity(){
		return city;
	}
}