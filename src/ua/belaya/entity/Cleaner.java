package ua.belaya.entity;

public class Cleaner extends Employee{
	private int amoutOfCleanedOffices;
	
	public Cleaner(String name, String surname, int age, double salary, int amoutOfCleanedOffices, Department city){
		super(name,surname,age,salary,city);
		this.amoutOfCleanedOffices = amoutOfCleanedOffices;
	}

    @Override
	public double calculateSalary(){
		return amoutOfCleanedOffices > 50 ? calculateSalary() + 100 : calculateSalary();
	}
} 